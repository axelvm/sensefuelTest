//selectors
const productDisplayer = document.querySelector(".product-displayer");
const addProduct = document.querySelector("button.button-add");
const removeProduct = document.querySelector("button.button-remove");

console.log(addProduct)
//event listener

window.addEventListener("load", renderProducts);
addProduct.addEventListener("click", addProductToBasket)
removeProduct.addEventListener("click", removeProductToBasket)

//functions
async function addProductToBasket(event) {
    event.preventDefault();
 console.log(event);
}

async function removeProductToBasket(event) {
    event.preventDefault();
    console.log(event);
}

async function renderProducts(event) {
    event.preventDefault();
    const products = await getProducts();
    console.log("loaded " + JSON.stringify(products))
    products.map((product) => {
        let productToRender = productCard(product);
        productDisplayer.appendChild(productToRender)
    })
}



async function getProducts() {
    return $.ajax({
        url: "http://localhost:3000/product",
        contentType: "application/json",
        headers: {
            Accept:"application/json",
            "Access-Control-Allow-Origin": "*"
        },
        dataType: 'json'
      });
    
}


function productCard(product){
    const productCard = document.createElement("div");
    productCard.classList.add("card");

    const productImage = document.createElement('img');
    productImage.classList.add("card-img");
    productImage.src=product.img;

    const productInformations = document.createElement('div');
    productInformations.classList.add('product-info-container');

    const productTitle = document.createElement('h2');
    productTitle.innerText = `${product.label} ${product.brand}`;
    productTitle.classList.add('title');

    const productPrice = document.createElement('p');
    productPrice.innerText = `${product.price} €`;
    productPrice.classList.add('product-info');

    const buttonDiv = document.createElement('div');
    buttonDiv.classList.add('button-div');

    const buttonAdd = document.createElement('button');
    buttonAdd.innerHTML = '<i class="fas fa-plus"></i>'
    buttonAdd.classList.add('button-add');

    const buttonRemove = document.createElement('button');
    buttonRemove.innerHTML = '<i class="fas fa-minus"></i>'
    buttonRemove.classList.add('button-remove');

    buttonDiv.appendChild(buttonAdd);
    buttonDiv.appendChild(buttonRemove);

    productCard.appendChild(productImage);
    productInformations.appendChild(productTitle);
    productInformations.appendChild(productPrice);
    productInformations.appendChild(buttonDiv)
    productCard.appendChild(productInformations);

    return productCard;
}