const dotenv = require('dotenv');

//To set path name you must modifie the name of application
dotenv.config({ path: `${process.env.NODEJS_CONF_PATH}/.env` });

module.exports = {
  //Config LOGGER
  logs: {
    path: process.env.NODEJS_LOGS_PATH || '/tmp',
    file: process.env.NODEJS_LOGS_FILE || '/',
    level: process.env.NODEJS_LOGS_LEVEL || 'info',
    console_level: process.env.NODEJS_LOGS_CONSOLE_LEVEL || 'info',
    max_size: process.env.NODEJS_LOGS_MAXSIZE || 5242880,
    max_file: process.env.NODEJS_LOGS_MAXFILE || 5,
    logs_file: process.env.NODEJS_LOGS_TO_FILE || false,
    response_api: `:remote-addr - :remote-user [:date[iso]] :method :url HTTP/:http-version :status :res[content-length] - :response-time ms :user-agent` //The format of output morgan logs
  },
  https_server: {
    enable_https: process.env.ENABLE_HTTPS || false,
    key_pem: process.env.KEY_PEM,
    server_crt: process.env.SERVER_CRT
  },
  //database configuration
  database: {},
  //api base path version
  bodyMaxSize: 10000
};