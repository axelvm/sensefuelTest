const productList = require('../database/productList.json'); //data product examples

/**
 * This function get product from fake database 
 * TODO: make a database call
 */
exports.getProducts = async() => {
    return productList
};

/**
 * This function get product from fake database 
 * TODO: make a database call
 */
exports.postProduct = async(body) => {
    const product = body;
    const validation = await this.validateProduct(product);
    if(validation.error) throw validation.error;
    return product;
};


/**
 * TODO: validate product body with Schema
 */
exports.validateProduct = (product) => {
    return true;
}
