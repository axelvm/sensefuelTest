const basketComplete = require('../database/basket.json');

/**
 * return basket : 1 for existing basket 
 * @param {string} id Id of the searched basket
 */
exports.getBasket = async(id) => {
    let initialBasket = {};
    if(id === '1') initialBasket = basketComplete;
    return initialBasket;
};

/**
 * return initialBasket
 * @param {Object} newBasket Basket tu upsert in database
 */
exports.insertBasket = async(newBasket, basketId) => {
    //TODO: insertion in database
    return newBasket;
};

/**
 * this function modify the basket before upsertion
 * @param {Number} quantity 
 * @param {String} productId 
 * @param {String} basketId 
 */
exports.modifyBasket = async (body, basketId) => {
    /*** Get basket in database ****/
    let basket = []
    if(basketId === '1') basket = basketComplete;
    /*******************************/
    let productIndex = basket.findIndex((product) => product.productId == body.productId);
    productIndex > -1 ? basket[productIndex].quantity += body.quantity : basket.push({productId: body.productId, quantity: body.quantity});
    //insertion in database
    await this.insertBasket(basket, basketId);
    return basket
}