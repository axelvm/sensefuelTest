"use strict";

const empty = require('is-empty');

/**
 * Class Response
 * 
 */
class Response {
    /**
     * initialize the api response
     * @param {String} processId 
     * @param {String} description 
     */
    constructor(data, statusCode, message) {
        this.data = data;
        this.statusCode = statusCode;
        this.message = message;
    }
}
module.exports = Response;