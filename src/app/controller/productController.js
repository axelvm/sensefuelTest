"use strict";
const HttpError = require('../../utils/errors/HttpError');
const logger = require('../../utils/logger');
const Response = require('../models/Response');
const { getProducts, postProduct } = require('../services/productService');
const {apiResponseMessage, apiResponseCode} = require('../../utils/constants');
/**
 * Controller template
 * 
 * @param {express.Request} req - express request
 * @param {express.Response} res - express response
 * @param {express.Next} next - express next middleware
 */
exports.postProductController = async(req, res, next) => {
  try{
    const response = await postProduct(req.body)
    res.status(apiResponseCode.CREATED).send(new Response(response, apiResponseCode.CREATED, apiResponseMessage.CREATED));
  } catch (error) {
    logger.error(error)
    res.status(apiResponseCode.INTERNAL_SERVER_ERROR).send(new HttpError(error, apiResponseCode.INTERNAL_SERVER_ERROR, apiResponseMessage.INTERNAL_ERROR))
  }
};


/**
 * Controller template
 * 
 * @param {express.Request} req - express request
 * @param {express.Response} res - express response
 * @param {express.Next} next - express next middleware
 */
exports.getProductController = async(req, res, next) => {
  try {
    const products = await getProducts();
    res.status(apiResponseCode.OK).send(products)
  } catch (error) {
    logger.error(error)
    res.status(apiResponseCode.INTERNAL_SERVER_ERROR).send(new HttpError(error, apiResponseCode.INTERNAL_SERVER_ERROR, apiResponseMessage.INTERNAL_ERROR))
  }

};