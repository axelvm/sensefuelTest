"use strict";
const HttpError = require('../../utils/errors/HttpError');
const logger = require('../../utils/logger');
const Response = require('../models/Response');
const { getBasket, modifyBasket } = require('../services/basketService');
const {apiResponseMessage, apiResponseCode} = require('../../utils/constants');
/**
 * Controller template
 * 
 * @param {express.Request} req - express request
 * @param {express.Response} res - express response
 * @param {express.Next} next - express next middleware
 */
exports.putBasketController = async(req, res, next) => {
  try{
    const response = await modifyBasket(req.body, req.params.id)
    res.status(apiResponseCode.OK).send(response)
  } catch (error) {    
    logger.error(error)
    res.status(apiResponseCode.INTERNAL_SERVER_ERROR).send(new HttpError(error, apiResponseCode.INTERNAL_SERVER_ERROR, apiResponseMessage.INTERNAL_ERROR))
  }
};


/**
 * Controller template
 * 
 * @param {express.Request} req - express request
 * @param {express.Response} res - express response
 * @param {express.Next} next - express next middleware
 */
exports.getBasketController = async(req, res, next) => {
  try {
    const basket = await getBasket(req.params.id);
    res.status(apiResponseCode.OK).send(basket)
  } catch (error) {
    logger.error(error)
    res.status(apiResponseCode.INTERNAL_SERVER_ERROR).send(new HttpError(error, apiResponseCode.INTERNAL_SERVER_ERROR, apiResponseMessage.INTERNAL_ERROR))
  }

};