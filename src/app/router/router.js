"use strict";

const express = require('express');
const router = express.Router();

const { postProductController, getProductController } = require('../controller/productController');
const { putBasketController, getBasketController } = require('../controller/basketController');
/**
 * @swagger
 * /product:
 *   post:
 *     tags:
 *       - product
 *     description: post product informations
 *     produces:
 *       - application/json
 *     requestBody:
 *       description: product body
 *       required: true
 *     responses:
 *       202:
 *         description: created
 *       400:
 *         description: bad criteria specified
 *       500:
 *         description: Internal server error
 */
router.post('/product', postProductController);


/**
 * @swagger
 * /product:
 *   get:
 *     tags:
 *       - product
 *     description: get product informations
 *     produces:
 *       - application/json
 *     requestBody:
 *       description: document to store
 *       required: true
 *     responses:
 *       200:
 *         description: OK
 *       400:
 *         description: bad criteria specified
 *       401:
 *         description: Unauthorized
 *       500:
 *         description: Internal server error
 */
router.get('/product', getProductController);

/**
 * @swagger
 * /basket:
 *   get:
 *     tags:
 *       - basket
 *     description: get product informations
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       400:
 *         description: bad criteria specified
 *       401:
 *         description: Unauthorized
 *       500:
 *         description: Internal server error
 */
router.get('/basket/:id', getBasketController);


/**
 * @swagger
 * /basket:
 *   get:
 *     tags:
 *       - basket
 *     description: put product informations
 *     produces:
 *       - application/json
 *     requestBody:
 *       description: document to store
 *       required: true
 *     responses:
 *       200:
 *         description: OK
 *       400:
 *         description: bad criteria specified
 *       401:
 *         description: Unauthorized
 *       500:
 *         description: Internal server error
 */
router.put('/basket/:id', putBasketController);

module.exports = router;