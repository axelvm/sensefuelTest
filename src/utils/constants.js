/**
 * date constants
 * @module date
 */
exports.date = {
    ISO_DATE_FORMAT: "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]",
    DATE_FORMAT: "YYYY-MM-DD",
    LOG_DATE_FORMAT: "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]",
    TIMEZONE: "Europe/Paris"
}

exports.apiResponseCode = {
    "OK": 200,
    "NOT_FOUND": 404,
    "UNAUTHORIZED": 401,
    "CREATED": 201,
    "BAD_REQUEST": 400,
    "UNSUPPORTED_MEDIA_TYPE": 415,
    "INTERNAL_SERVER_ERROR": 500,
    "REQUEST_TOO_LONG": 413
};

exports.apiResponseMessage = {
    "INTERNAL_ERROR": "INTERNAL_ERROR",
    "BAD_REQUEST": "BAD_REQUEST",
    "NOT_FOUND": "NOT_FOUND",
    "ILLEGAL_ARGUMENT": "ILLEGAL_ARGUMENT",
    "QUERY_ERROR": "QUERY_ERROR",
    "DATABASE_CONNECTION": "DATABASE_CONNECTION",
    "LDAP_CONNECTION": "LDAP_CONNECTION",
    "RECORD_NOT_FOUND": "RECORD_NOT_FOUND",
    "REQUEST_TOO_LONG": "REQUEST_TOO_LONG",
    "OK": "OK",
    "DUPLICATE": "DUPLICATE",
    "VALIDATION_ERROR": "VALIDATION_ERROR",
    "UNAUTHORIZED": "UNAUTHORIZED",
    "WARN": "WARN",
    "CREATED": "CREATED",
    "UNSUPPORTED_MEDIA_TYPE": "UNSUPPORTED_MEDIA_TYPE",
    "ACKNOWLEDGED": "The operation request has been acknowledged and is now being processed",
    "DELETE_ACKNOWLEDGED": "The deletion request has been acknowledged and is now being processed",
    "FAILED": "The data cannot be integrated due to the failures below"
};