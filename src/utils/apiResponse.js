"use strict";


// ApiResponse class : will return a standard json response  

module.exports = class ApiResponse {
    constructor(statut, code, message, more_info) {
        this.status = statut; //ApiReponseStatut 
        this.code = code; //ApiResponseCode
        this.message = message;
        this.more_info = more_info;
    }
};