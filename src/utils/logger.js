"use strict";
const empty = require('is-empty');
const winston = require('winston');
const { combine, errors, simple, colorize, timestamp } = winston.format;
const path = require('path');
const { getNamespace } = require('cls-hooked');

const config = require('../config/config');

/**
 * Wrap Winston logger to print reqId in each log
 * @param {*} message message to log
 */
const formatMessage = winston.format.printf(info => {
    let ns = getNamespace('correlation-id');
    if (!empty(ns)) {
        info.correlationId = ns.get('reqId');
    }
    return info;
});


const transports = [
    new winston.transports.Console({
        level: config.logs.console_level,
        handleExceptions: true,
        format: combine(colorize(), simple())
    })
];
if (config.logs.logs_file) {
    transports.push(new winston.transports.File({
        format: combine(
            simple(),
        ),
        filename: path.resolve(__filename, `${config.logs.path}${config.logs.file}`),
        maxsize: config.logs.max_size,
        timestamp: true,
        maxFiles: config.logs.max_file,
        eol: '\r\n',
        prettyPrint: true,
        zippedArchive: false,
    }));
}
const winstonLogger = winston.createLogger({
    level: 'debug',
    format: combine(
        formatMessage,
        timestamp(),
        errors({ stack: true })
    ),
    transports
});

module.exports = winstonLogger;
module.exports.stream = {
    write: (message) => {
        winstonLogger.info(message.slice(0, -1));

    }
};