// Based on https://gist.github.com/slavafomin/b164e3e710a6fc9352c934b9073e7216
module.exports = class AppError extends Error {
    constructor(message, code) {
        // Calling parent constructor of base Error class.
        super(message);
        // Saving class name in the property of our custom error as a shortcut.
        this.name = this.constructor.name;
        // Custom application error code
        // For example: "HTTP_ERROR"
        this.code = code || "";
    }
};