// Custom error used to handle http errors
module.exports = class HttpError extends require("./AppError") {
    constructor(message, httpStatusCode, errorCode) {
        // message will be sent in the response body
        // see app/errors/middlewares.js
        super(message, "HTTP_ERROR");

        // Http status code of the error
        // For example: 400, 404, 500
        this.httpStatusCode = httpStatusCode || 500;

        // Error code sent in the response body
        // Gives more info than the http status code
        // For example: "route_not_found" (404), "invalid_ldap_credentials" (401)
        this.errorCode = errorCode;
    }
};