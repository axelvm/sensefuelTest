const HttpError = require("../errors/HttpError"),
    apiResponse = require("./../apiResponse"),
    { apiResponseCode, apiResponseMessage } = require("./../constants"),
    logger = require('./../../utils/logger');


// Not Found Error Handler
// Mark it as 404 and pass it to the next error middleware
exports.notFound = (req, res, next) => {
    const error = new HttpError(
        `Route ${req.method} ${req.path} not found.`,
        apiResponseCode.NOT_FOUND,
        "route_not_found"
    );
    next(error);
};

// Express error middlewares need 4 arguments
// eslint-disable-next-line no-unused-vars
exports.lastErrorHandler = (err, req, res, next) => {
    let currentLocation = `${req.method} ${req.originalUrl}`;
    // console.trace(err);
    // See "Default Error Handler": http://expressjs.com/en/guide/error-handling.html
    if (res.headersSent) {
        return next(err);
    }
    // Create the http error that will be sent
    let httpError;
    if (err instanceof HttpError) {
        httpError = err;
    } else {
        // If err is not a HttpError, create a 500 HttpError
        // Show error message only in development environment
        // const errorMessage = req.app.get("env") === "development" ? err.message : "";
        httpError = new HttpError(err.message, apiResponseCode.INTERNAL_SERVER_ERROR, apiResponseMessage.INTERNAL_ERROR);
    }

    const isServerError = httpError.httpStatusCode >= apiResponseCode.INTERNAL_SERVER_ERROR;

    //Log any server errors
    if (isServerError) {
        logger.error(err);
    }

    let statusCode = httpError.httpStatusCode;
    let status;
    if (httpError.errorCode === apiResponseMessage.INTERNAL_ERROR) {
        status = apiResponseCode.INTERNAL_SERVER_ERROR;
    } else if (httpError.errorCode === apiResponseMessage.NOT_FOUND) {
        status = apiResponseCode.NOT_FOUND;
    } else {
        status = apiResponseCode.BAD_REQUEST;
    }
    //body parser error
    if (err.type === 'entity.too.large' && err.status === 413) {
        status = apiResponseCode.REQUEST_TOO_LONG;
        statusCode = apiResponseCode.REQUEST_TOO_LONG;
        httpError.errorCode = apiResponseMessage.REQUEST_TOO_LONG;
    }
    /**
     * if ou would like send the stack you can use this : errorStack: req.app.get("env") === "development" && isServerError ?
            httpError.stack : undefined
     */
    let response = new apiResponse(httpError.errorCode, status, httpError.message, currentLocation);
    res.status(statusCode);
    res.json(response);
};