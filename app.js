"use strict";
const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const compression = require('compression');

const loggers = require('./src/utils/logger');

//middlewares error
const errorsMiddlewares = require('./src/utils/errors/middlewares');
const config = require('./src/config/config');
const apiRouter = require('./src/app/router/router');
const packageJSON = require('./package');


const app = express();

require('express-async-await')(app);
//api version

loggers.info("Starting 'Express'");
loggers.info(`project version: ${packageJSON.version}`);

process.on('unhandledRejection', (reason) => {
  loggers.error("Error:", reason);
});
process.on('uncaughtException', (reason) => {
  loggers.error("Error: ", reason);
});
// view engine setup
app.set('x-powered-by', false);
//CORS middleware
const allowCrossDomain = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, application/json, charset=utf-8, X-Requested-With');
  next();
};

// setImmediate(async() => {
// });

app.use(compression());
app.use(logger(config.logs.response_api, { stream: loggers.stream }));
app.use(bodyParser.json({ limit: config.bodyMaxSize }));
app.use(bodyParser.urlencoded({ limit: config.bodyMaxSize, extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(allowCrossDomain);

app.use(apiRouter);
// app.use('/', swaggerRoutes);

// Handle errors
app.use(errorsMiddlewares.notFound);
app.use(errorsMiddlewares.lastErrorHandler);

module.exports = app;